import sys
import discord
from replies import BotReply

client = discord.Client()

BOT_TOKEN = ""
replies = BotReply()


@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    channel = message.channel

    if message.content.startswith('!hello'):
        msg = 'Hello {0.author.mention}'.format(message)
        await channel.send(msg)

    else:
        reply = replies.get_reply_for_message(message)
        if reply:
            await channel.send(reply)


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')


def get_commandline_arguments():
    try:
        commandline_token = sys.argv[1]
    except Exception:
        commandline_token = None
    return commandline_token

cmdline_token = get_commandline_arguments()
if cmdline_token:
    client.run(cmdline_token)
elif BOT_TOKEN:
    client.run(BOT_TOKEN)
else:
    print("Please either give your bot token as a commandline argument or add your token into BOT_TOKEN at the top of gaming_bot.py")
