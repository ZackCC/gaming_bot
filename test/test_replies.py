import unittest
from replies import BotReply


class AuthorDummy(object):

    def __init__(self, author=None, display_name=None):
        self.author = author
        self.display_name = display_name if display_name else author


class MessageDummy(object):

    def __init__(self, content="", author=None):
        self.content = content
        self.author = AuthorDummy(author)


class TestReplies(unittest.TestCase):
    bot_replies = BotReply()

    def test_username_is_formatted_correctly(self):
        user = "Username(*¯︶¯*)"
        message = MessageDummy("gaming", user)
        reply = self.bot_replies.get_reply_for_message(message)
        assert user in reply

    def test_reply_for_blocking(self):
        user = "Username(*¯︶¯*)"
        message = MessageDummy("imagine blocking", user)
        reply = self.bot_replies.get_reply_for_message(message)
        assert reply is not None

        message = MessageDummy("please block on wake up!", user)
        reply = self.bot_replies.get_reply_for_message(message)
        assert reply is not None

    def test_reply_for_gaming(self):
        user = "Username(*¯︶¯*)"
        message = MessageDummy("gaming", user)
        reply = self.bot_replies.get_reply_for_message(message)
        assert reply == "Ah, {} is...gaming...".format(user)

        message = MessageDummy("no gaming", user)
        reply = self.bot_replies.get_reply_for_message(message)
        assert reply == "Ah, {} is...not gaming...".format(user)

        message = MessageDummy("not gaming", user)
        reply = self.bot_replies.get_reply_for_message(message)
        assert reply == "Ah, {} is...not gaming...".format(user)


if __name__ == "__main__":
    unittest.main()
