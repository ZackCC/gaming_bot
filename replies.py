import os
import random


class BotReply(object):

    def __init__(self):
        self.blocking_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "resources", "blocking.txt")

    def get_reply_for_message(self, message):
        reply = None

        if message.content.lower() == "gaming":
            user = message.author.display_name
            reply = "Ah, {} is...gaming...".format(user)

        elif message.content.lower() == "no gaming" or message.content.lower() == "not gaming":
            user = message.author.display_name
            reply = "Ah, {} is...not gaming...".format(user)

        elif "block" in message.content.lower():
            lines = self._get_lines_from_file(self.blocking_file)
            value = random.randint(0, len(lines) - 1)
            reply = lines[value]

        # Add more conditions here and create a reply

        return reply

    def _get_lines_from_file(self, my_file):
        with open(my_file, "r", encoding="utf-8") as f:
            lines = f.read().splitlines()
        return [i for i in lines if i]
