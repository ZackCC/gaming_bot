# gaming_bot
Bot that replies "Ah, {user} is...gaming..." whenever "gaming" is mentioned in the chat

## Installation
You'll need Python (tested with version 3.6.5)
Discord library. (Where the f- did I get it? "pip install discord" maybe works)

## Usage
Add your bot token into gaming_bot.py.
To start your bot, run command:
    - python gaming_bot

## Contributing
Do whatever you want lol

## Project status
Initial commit done
